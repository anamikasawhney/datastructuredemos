﻿using static System.Net.Mime.MediaTypeNames;

namespace LinkedListDemo
{
    class Node
    {
        public int Data;
        public Node next;
    }
    internal class Program
    {
        static Node newNode, start = null, last = null, ptr, prev;
        static void Main(string[] args)
        {
            string ch = "y";
            while (ch.ToLower() == "y")
            {
                switch (Menu())
                {
                    case 1:
                        {
                            Console.WriteLine("Enter element to insert");
                            int num = byte.Parse(Console.ReadLine());
                            InsertData(num);
                            
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enter element to delete");
                            int num = byte.Parse(Console.ReadLine());
                            DeleteData(num);
                            
                            break;

                        }
                    case 3:
                        {
                            DisplayData();
                            break;
                        }

                }
                Console.WriteLine("Want to repeat");
                ch = Console.ReadLine();
            }


        }
        static int Menu()
        {
            Console.WriteLine("1. Enqueue ");
            Console.WriteLine("2. Dequeue");
            Console.WriteLine("3. Display ELements");
            Console.WriteLine("Enter your choice");
            int ch = byte.Parse(Console.ReadLine());
            return ch;
        }
        static void InsertData(int num)
        {
           
            newNode = new Node();
            newNode.Data = num;
            newNode.next = null;

            if(start == null)
            {
                last=start=newNode;
                Console.WriteLine("STart node has been added");
            }
            else
            {
                if (num <= start.Data)
                {
                    newNode.next = start;
                    start = newNode;
                    Console.WriteLine("Added in beg");
                }
                else if (num >= last.Data)
                {
                    for (ptr = start; ptr.next != null; ptr = ptr.next) ;

                    ptr.next = newNode;
                    newNode.next = null;
                    last = newNode;
                    Console.WriteLine("Aded in last");
                }
                else
                {
                    for (prev = ptr = start; ptr != null; prev = ptr, ptr = ptr.next)
                    {
                        if (num >= prev.Data && num < ptr.Data)
                        {
                            prev.next = newNode;
                            newNode.next = ptr;
                            break;
                        }


                    }
                }
            }
             

        }
        static void DeleteData(int num)
        {
            if (num == start.Data)
            {
                start = start.next;
            }
            else if (num == last.Data)
            {
                for (prev = ptr = start; ptr.next != null; prev = ptr, ptr = ptr.next) ;
                prev.next = null;
                last = prev;
            }
            else
            {
                for (prev = ptr = start; ptr!= null; prev = ptr, ptr = ptr.next)
                {
                    if(num== ptr.Data)
                    {
                        prev.next = ptr.next;
                        break;
                    }
                }




            }

        }
      
        static void DisplayData()
        {
            for(ptr=start;ptr!=null;ptr=ptr.next)
            {
                Console.WriteLine(ptr.Data);
            }
    }
    }
}
