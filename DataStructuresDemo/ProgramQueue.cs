﻿using System;
using System.Data;
using System.Security.Cryptography.X509Certificates;

namespace DataStructuresDemo
{
    internal class ProgramQueue
    {
        static int front = -1, rear =-1;
        static int[] queue = new int[10];
        static void Main(string[] args)
        {
            string ch = "y";
            while (ch.ToLower() == "y")
            {
                switch (Menu())
                {
                    case 1:
                        {
                            Console.WriteLine("Enter element to insert");
                            int num = byte.Parse(Console.ReadLine());
                            Enqueue(num);
                            Console.WriteLine($"Front is now {front}  and Rear is {rear}");
                            break;
                        }
                    case 2:
                        {
                            Dequeue();
                            Console.WriteLine($"Front is now {front}  and Rear is {rear}");
                            break;

                        }
                    case 3:
                        {
                            DisplayElements();
                            break;
                        }

                }
                Console.WriteLine("Want to repeat");
                ch = Console.ReadLine();
            }


            }
        static int Menu()
        {
            Console.WriteLine("1. Enqueue ");
            Console.WriteLine("2. Dequeue");
            Console.WriteLine("3. Display ELements");
            Console.WriteLine("Enter your choice");
            int ch = byte.Parse(Console.ReadLine());
            return ch;
        }
        static void Enqueue(int num)
        {
            if ((rear > queue.Length - 1) || (front > rear))
                Console.WriteLine("Overflow, cant insert more elements");
            else if (front == rear && front == -1)
            {
                front = 0;
                rear = 0;
                queue[front] = num;
            }
            else
            {
                queue[++rear] = num;
            }
        }
          
        static void Dequeue()
        {
            if (front < 0 || front>rear)

                Console.WriteLine("Underflow");
            else
                front++;
             
        }
        static void DisplayElements()
        {
            if (front == rear && rear == -1) Console.WriteLine("queue is empty");
            else
            {
                for (int i = front; i <= rear; i++)
                    Console.WriteLine(queue[i]);
            }
        }
    }
}
