﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace DataStructuresDemo
{
    internal class Program
    {
        static int top = -1;
        static int[] stack = new int[10];
        static void Main(string[] args)
        {
            string ch = "y";
            while (ch.ToLower() == "y")
            {
                switch (Menu())
                {
                    case 1:
                        {
                            Console.WriteLine("Enter element to insert");
                            int num = byte.Parse(Console.ReadLine());
                            Push(num);
                            Console.WriteLine("Top is now " + top);
                            break;
                        }
                    case 2:
                        {
                            Pop();
                            Console.WriteLine("Top is now " + top);
                            break;

                        }
                    case 3:
                        {
                            DisplayElements();
                            break;
                        }

                }
                Console.WriteLine("Want to repeat");
                ch = Console.ReadLine();
            }


            }
        static int Menu()
        {
            Console.WriteLine("1. Push ");
            Console.WriteLine("2. Pop");
            Console.WriteLine("3. Display ELements");
            Console.WriteLine("Enter your choice");
            int ch = byte.Parse(Console.ReadLine());
            return ch;
        }
        static void Push(int num)
        {
              if (top > stack.Length - 1)
                Console.WriteLine("Overflow, cant insert more elements");
            else
                stack[++top] = num;
            
        }
        static void Pop()
        {
            if (top < 0)

                Console.WriteLine("Underflow");
            else
                top--;
             
        }
        static void DisplayElements()
        {
            for(int i = top;i>=0;i--)
                Console.WriteLine(stack[i]);
        }
    }
}
